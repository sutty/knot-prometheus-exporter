require "yaml"
require "./macros"

class Stats
  include YAML::Serializable

  class Server
    include YAML::Serializable

    @[YAML::Field(key: "zone-count")]
    property zone_count : UInt8
  end

  class ModCookies
    include YAML::Serializable

    @[YAML::Field(key: "presence")]
    property cookies_presence : UInt64
  end

  class ModStats
    include YAML::Serializable

    class RequestProtocol
      include YAML::Serializable

      property udp4 : UInt64
      property tcp4 : UInt64
      property udp6 : UInt64
      property tcp6 : UInt64
    end

    class ServerOperation
      include YAML::Serializable

      property query : UInt64?
      property update : UInt64?
      property notify : UInt64?
      property axfr : UInt64?
      property ixfr : UInt64?
      property invalid : UInt64?
    end

    class RequestBytes
      include YAML::Serializable

      property query : UInt64?
      property update : UInt64?
      property other : UInt64?
    end

    class ResponseBytes
      include YAML::Serializable

      property reply : UInt64?
      property transfer : UInt64?
      property other : UInt64?
    end

    class EdnsPresence
      include YAML::Serializable

      property request : UInt64?
      property response : UInt64?
    end

    class FlagPresence
      include YAML::Serializable

      @[YAML::Field(key: "TC")]
      property tc_flag : UInt64?

      @[YAML::Field(key: "DO")]
      property do_flag : UInt64?
    end

    class ResponseCode
      include YAML::Serializable

      @[YAML::Field(key: "NOERROR")]
      property noerror : UInt64?

      @[YAML::Field(key: "FORMERR")]
      property formerr : UInt64?

      @[YAML::Field(key: "SERVFAIL")]
      property servfail : UInt64?

      @[YAML::Field(key: "NXDOMAIN")]
      property nxdomain : UInt64?

      @[YAML::Field(key: "NOTIMPL")]
      property notimpl : UInt64?

      @[YAML::Field(key: "REFUSED")]
      property refused : UInt64?

      @[YAML::Field(key: "YXDOMAIN")]
      property yxdomain : UInt64?

      @[YAML::Field(key: "YXRRSET")]
      property yxrrset : UInt64?

      @[YAML::Field(key: "NXRRSET")]
      property nxrrset : UInt64?

      @[YAML::Field(key: "NOTAUTH")]
      property notauth : UInt64?

      @[YAML::Field(key: "NOTZONE")]
      property notzone : UInt64?

      @[YAML::Field(key: "BADVERS")]
      property badvers : UInt64?

      @[YAML::Field(key: "BADKEY")]
      property badkey : UInt64?

      @[YAML::Field(key: "BADTIME")]
      property badtime : UInt64?

      @[YAML::Field(key: "BADMODE")]
      property badmode : UInt64?

      @[YAML::Field(key: "BADNAME")]
      property badname : UInt64?

      @[YAML::Field(key: "BADALG")]
      property badalg : UInt64?

      @[YAML::Field(key: "BADTRUNC")]
      property badtrunc : UInt64?

      @[YAML::Field(key: "BADCOOKIE")]
      property badcookie : UInt64?

      property other : UInt64?
    end

    # @see {knot-2.9.7/src/libknot/descriptor.c}
    class EdnsOption
      include YAML::Serializable

      @[YAML::Field(key: "CODE0")]
      property code0 : UInt64?

      @[YAML::Field(key: "LLQ")]
      property llq : UInt64?

      @[YAML::Field(key: "UL")]
      property ul : UInt64?

      @[YAML::Field(key: "NSID")]
      property nsid : UInt64?

      @[YAML::Field(key: "DAU")]
      property dau : UInt64?

      @[YAML::Field(key: "DHU")]
      property dhu : UInt64?

      @[YAML::Field(key: "N3U")]
      property n3u : UInt64?

      @[YAML::Field(key: "EDNS-CLIENT-SUBNET")]
      property edns_client_subnet : UInt64?

      @[YAML::Field(key: "EDNS-EXPIRE")]
      property edns_expire : UInt64?

      @[YAML::Field(key: "COOKIE")]
      property cookie : UInt64?

      @[YAML::Field(key: "EDNS-TCP-KEEPALIVE")]
      property edns_tcp_keepalive : UInt64?

      @[YAML::Field(key: "PADDING")]
      property padding : UInt64?

      @[YAML::Field(key: "CHAIN")]
      property chain : UInt64?

      @[YAML::Field(key: "EDNS-KEY-TAG")]
      property edns_key_tag : UInt64?

      property other : UInt64?
    end

    class ReplyNodata
      include YAML::Serializable

      @[YAML::Field(key: "A")]
      property a : UInt64?

      @[YAML::Field(key: "AAAA")]
      property aaaa : UInt64?

      property other : UInt64?
    end

    # @see {knot-2.9.7/src/libknot/descriptor.h}
    class QueryType
      include YAML::Serializable

      define_query_types(["a","ns","type3","type4","cname","soa","type7","type8","type9","null","type11","ptr","hinfo","minfo","mx","txt","rp","afsdb","type19","type20","rt","type22","type23","sig","key","type26","type27","aaaa","loc","type30","type31","type32","srv","type34","naptr","kx","cert","type38","dname","type40","opt","apl","ds","sshfp","ipseckey","rrsig","nsec","dnskey","dhcid","nsec3","nsec3param","tlsa","smimea","type54","type55","type56","type57","type58","cds","cdnskey","openpgpkey","csync","zonemd","type64","type65","type66","type67","type68","type69","type70","type71","type72","type73","type74","type75","type76","type77","type78","type79","type80","type81","type82","type83","type84","type85","type86","type87","type88","type89","type90","type91","type92","type93","type94","type95","type96","type97","type98","spf","type100","type101","type102","type103","nid","l32","l64","lp","eui48","eui64","type110","type111","type112","type113","type114","type115","type116","type117","type118","type119","type120","type121","type122","type123","type124","type125","type126","type127","type128","type129","type130","type131","type132","type133","type134","type135","type136","type137","type138","type139","type140","type141","type142","type143","type144","type145","type146","type147","type148","type149","type150","type151","type152","type153","type154","type155","type156","type157","type158","type159","type160","type161","type162","type163","type164","type165","type166","type167","type168","type169","type170","type171","type172","type173","type174","type175","type176","type177","type178","type179","type180","type181","type182","type183","type184","type185","type186","type187","type188","type189","type190","type191","type192","type193","type194","type195","type196","type197","type198","type199","type200","type201","type202","type203","type204","type205","type206","type207","type208","type209","type210","type211","type212","type213","type214","type215","type216","type217","type218","type219","type220","type221","type222","type223","type224","type225","type226","type227","type228","type229","type230","type231","type232","type233","type234","type235","type236","type237","type238","type239","type240","type241","type242","type243","type244","type245","type246","type247","type248","tkey","tsig","ixfr","axfr","type253","type254","any","uri","caa","type258","type259","type260","other","unknown"])
    end

    # Query size goes in groups of 16 bytes from 0 to 287 and then from
    # 288 to 4096
    # class QuerySize
    # include YAML::Serializable
    #
    # @[YAML::Field(key: "0-15")]
    # property from_0_to_15 : UInt64?
    # end

    # Reply size goes in groups of 16 bytes from 0 to 4095 (256 groups)
    # and then from 4096 to 65535
    # class ReplySize
    #  include YAML::Serializable
    #
    #  @[YAML::Field(key: "0-15")]
    #  property from_0_to_15 : UInt64?
    # end

    @[YAML::Field(key: "request-protocol")]
    property request_protocol : RequestProtocol

    @[YAML::Field(key: "server-operation")]
    property server_operation : ServerOperation

    @[YAML::Field(key: "request-bytes")]
    property request_bytes : RequestBytes

    @[YAML::Field(key: "response-bytes")]
    property response_bytes : ResponseBytes

    @[YAML::Field(key: "edns-presence")]
    property edns_presence : EdnsPresence

    @[YAML::Field(key: "flag-presence")]
    property flag_presence : FlagPresence

    @[YAML::Field(key: "response-code")]
    property response_code : ResponseCode

    @[YAML::Field(key: "request-edns-option")]
    property request_edns_option : EdnsOption

    @[YAML::Field(key: "response-edns-option")]
    property response_edns_option : EdnsOption

    @[YAML::Field(key: "reply-nodata")]
    property reply_nodata : ReplyNodata

    @[YAML::Field(key: "query-type")]
    property query_type : QueryType

#   @[YAML::Field(key: "query-size")]
#   property query_size : QuerySize
  end

  class ModRrl
    include YAML::Serializable

    property slipped : UInt64
    property dropped : UInt64
  end

  property server : Server

  @[YAML::Field(key: "mod-cookies")]
  property mod_cookies : ModCookies

  @[YAML::Field(key: "mod-stats")]
  property mod_stats : ModStats

  @[YAML::Field(key: "mod-rrl")]
  property mod_rrl : ModRrl
end
