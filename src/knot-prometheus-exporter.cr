require "http/server"
require "http/server/handlers/compress_handler"
require "crometheus/registry"
require "./stats_handler"

stats = ARGV.size > 1 ? ARGV[0] : "/tmp/stats.yaml"

unless File.exists?(stats)
  puts "#{stats} doesn't exist"
  exit 1
end

metrics_handler = Crometheus.default_registry.get_handler
Crometheus.default_registry.path = "/metrics"

# Serve metrics, everytime /metrics is visited we read stats.yaml 
handlers = [HTTP::CompressHandler.new, HTTP::LogHandler.new, HTTP::ErrorHandler.new(true), StatsHandler.new(stats), metrics_handler]
server = HTTP::Server.new(handlers) do |context|
end

address = server.bind_tcp 5000

puts "Serving metrics at http://#{address}/metrics"
puts "Reading stats from #{stats}"
puts "Press Ctrl+C to exit"
server.listen
