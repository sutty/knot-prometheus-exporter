macro define_query_types(types)
  {% for type in types %}
    @[YAML::Field(key: {{ type.upcase }})]
    property {{ type.id }} : UInt64?
  {% end %}
end

macro define_gauges(stats, root)
  {% for stat, values in stats %}
    {% for value in values %}
      {% if root %}
        @gauge[:knot_{{ root.id }}_{{ stat.id }}_{{ value.id }}] = Crometheus::Gauge.new(:knot_{{ root.id }}_{{ stat.id }}_{{ value.id }}, "{{ stat.id }} {{ value.id }}")
      {% else %}
        @gauge[:knot_{{ stat.id }}_{{ value.id }}] = Crometheus::Gauge.new(:knot_{{ stat.id }}_{{ value.id }}, "{{ stat.id }} {{ value.id }}")
      {% end %}
    {% end %}
  {% end %}
end

macro set_metrics(stats, root)
  {% for stat, values in stats %}
    {% if root %}
      unless stats.{{ root.id }}.nil? || stats.{{ root.id }}.{{ stat.id }}.nil?
        {% for value in values %}
          unless stats.{{ root.id }}.{{ stat.id }}.{{ value.id }}.nil?
            @gauge[:knot_{{ root.id }}_{{ stat.id }}_{{ value.id }}].set stats.{{ root.id }}.{{ stat.id }}.{{ value.id }} || 0
          end
        {% end %}
      end
    {% else %}
      if stats.{{ stat.id }}
        {% for value in values %}
          unless stats.{{ stat.id }}.{{ value.id }}.nil?
            @gauge[:knot_{{ stat.id }}_{{ value.id }}].set stats.{{ stat.id }}.{{ value.id }} || 0
          end
        {% end %}
      end
    {% end %}
  {% end %}
end
