source := $(wildcard src/*.cr)

all: knot-prometheus-exporter
knot-prometheus-exporter: src/knot-prometheus-exporter.cr $(source)
	crystal build --release $<
	strip --strip-all $@
