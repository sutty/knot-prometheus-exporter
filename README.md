# knot-prometheus-exporter

This exporter reads the `stats.yaml` file and converts it into
Prometheus metrics.

Note `query-size` and `response-size` stats aren't supported yet.

## Installation

```bash
crystal build --release src/knot-prometheus-exporter
strip --strip-all knot-prometheus-exporter
```

## Usage

Configure Knot to export stats:

```yaml
server:
  rundir: "/tmp"

mod-stats:
- id: "default"
  edns-presence: true
  flag-presence: true
  request-edns-option: true
  response-edns-option: true
  reply-nodata: true
  query-type: true

template:
- global-module: "mod-stats/default"
```

Run with:

```bash
knot-prometheus-exporter /tmp/stats.yaml
```

And configure Prometheus to scrape from:

```yaml
scrape_configs:
  - job_name: 'knot'
    scrape_interval: 1m
    scrape_timeout: 1m
    static_configs:
    - targets:
      - 'http://localhost:5000'
```

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/knot-prometheus-exporter>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The program is available as free software under the terms of the GPL3
License.

## Code of Conduct

Everyone interacting in the jekyll-onion-location project’s codebases,
issue trackers, chat rooms and mailing lists is expected to follow the
[code of conduct](https://sutty.nl/en/code-of-conduct/).
